# DL_project_2021_first

[program](https://docs.google.com/document/d/1MzdbmrhabHUIb5-I-WGi_PSe_8J25_utfKjmfKUtfAc/edit)

## Task 
Задача 7* Генерация описаний к изображениям (image captioning) (группа 2)
Датасет - coco
https://www.tensorflow.org/datasets/catalog/coco_captions?hl=nb

## Members
1. **Русин Ярослав**
2. Коффи Эвелине
3. Фида Александр
4. Репин Сергей

## Workspace
> [trello](https://trello.com/b/lXawEUG9/dlproject)
